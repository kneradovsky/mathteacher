import 'package:flutter/material.dart';

class StackedPile extends StatelessWidget {
  final Color color;
  final int initialCount;
  final Size size;
  final int countLimit;
  final Widget child;

  StackedPile(
      {Key key, this.child, this.color, this.initialCount = 0, this.countLimit=10, this.size = const Size(
          100, 20)}) : super(key: key) ;


  @override
  Widget build(BuildContext context) {
    var pilecount = initialCount>countLimit ? countLimit : initialCount<0 ? 0 : initialCount;
    List<Widget> children = new List();

    for(int i=0;i<pilecount;i++) {
      children.add(Positioned(
          child: buildCard(context),
          top: 2*i.toDouble(),
          left: 2*i.toDouble()
      ));
    }
    children.add(
      Container(
        width: size.width+(2*(pilecount).toDouble()),
        height: size.height+(2*(pilecount).toDouble()),
        alignment: Alignment.bottomRight,
        child: buildCard(context,child:child)
      )
    );

    return Container(
      width: size.width+2*countLimit,
      height: size.height+2*countLimit,
      child: Stack(
        children: children
      ),
    );
  }


  Widget buildCard(BuildContext context,{Widget child}) {
    return Container(
      width: size.width,
      height: size.height,
      decoration: ShapeDecoration(
          shape: RoundedRectangleBorder(
              side: BorderSide(color: Theme
                  .of(context)
                  .cardColor),
              borderRadius: BorderRadius.circular(5)

          ),
          color: this.color
      ),
      child: child,
    );
  }

}


