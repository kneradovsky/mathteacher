import 'package:flutter/material.dart';

class RoundedShadowButton extends StatelessWidget {
  final Widget child;
  final Function onPressed;
  final Color baseColor;
  final Color shadowColor;
  final double width,height;

  RoundedShadowButton({Key key,
    @required this.child,
    this.baseColor = Colors.grey,
    this.shadowColor = Colors.grey,
    this.width=300,
    this.height=100,
    this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var mqdata = MediaQuery.of(context);
    double width = mqdata.size.width - 20 * 2;
    width = width > this.width ? this.width : width;
    double height = mqdata.size.height / 6 - 10 * 2;
    height = height > this.height ? this.height : height;
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          boxShadow: [
            BoxShadow(color: shadowColor, offset: Offset(5, 5), blurRadius: 4.0)
          ]
      ),
      child: RaisedButton(
          onPressed: onPressed,
          color: baseColor,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15)),
          child: Container(
              child: child)
      ),
    );
  }
}

