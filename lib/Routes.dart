import 'package:flutter/material.dart';
import 'package:mathteacher/ComingSoon.dart';
import 'package:mathteacher/SelectSettingsPage.dart';
import 'package:mathteacher/SettingsPage.dart';
import 'package:mathteacher/StartPage.dart';
import 'package:mathteacher/StatsPage.dart';
import 'package:mathteacher/TrainPage.dart';


final String homeRoute = "/";
final String trainRoute = "/train";
final String setOpersRoute = "/setOperations";
final String setTimeOrTriesRoute = "/setTimeOrTries";
final String statsRoute = "/stats";
final String statrecsRoute = "/stats/records";
final String comingSoonRoute = "/comingSoon";

Map<String,WidgetBuilder Function(Object argument)> routes = {
  homeRoute : (args) => (context) => StartPage(),
  trainRoute: (args) => (context) => TrainPage(),
  setOpersRoute: (args) => (context) => SelectSettingsPage(),
  setTimeOrTriesRoute: (args) => (context) => SelectSettingsPage(),
  statsRoute: (args) => (context) => StatsPage(),
  statrecsRoute: (args) => (context) => StatsRecordsPage(args:args),
  comingSoonRoute: (args) => (context) => ComingSoon(),
};

Route<dynamic> onGenerateCountRoute(RouteSettings rs) {
  if(routes.containsKey(rs.name)) return MaterialPageRoute(builder: routes[rs.name](rs.arguments),settings: rs);
  return null;
}