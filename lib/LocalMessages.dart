import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart';

import 'i18n/messages_all.dart';


class CountThisLocalizationsDelegate extends LocalizationsDelegate<CountThisLocalizations> {
  const CountThisLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en','ru'].contains(locale.languageCode);

  @override
  Future<CountThisLocalizations> load(Locale locale) => CountThisLocalizations.load(locale);

  @override
  bool shouldReload(LocalizationsDelegate<CountThisLocalizations> old) => false;
}

class CountThisLocalizations {
  static Future<CountThisLocalizations> load(Locale locale) {
    final String name = locale.countryCode==null || locale.countryCode.isEmpty
        ? locale.languageCode
        : locale.toString();
    print(locale.toString());
    final String localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      return CountThisLocalizations();
    });
  }
  static CountThisLocalizations of(BuildContext context) {
    return Localizations.of<CountThisLocalizations>(context,CountThisLocalizations);
  }

  String formatBooleanYesNo(bool val) {
    return val ? lblYes : lblNo;
  }

  String get lblYes {
    return Intl.message('Yes',
        name: 'lblYes',
        desc: 'Yes label');
  }

  String get lblNo {
    return Intl.message('No',
        name: 'lblNo',
        desc: 'No label');
  }

  String get lblOK {
    return Intl.message('OK',
        name: 'lblOK',
        desc: 'OK label');
  }


  String get appTitle {
    return Intl.message('Calc this!!!',
    name: 'appTitle',
    desc: 'main window title');
  }

  String get btnTrialStart {
    return Intl.message('Start',
        name: 'btnTrialStart',
        desc: 'Start solution trial');
  }
  String get btnTrialStop {
    return Intl.message('Stop',
        name: 'btnTrialStop',
        desc: 'Start solution trial');
  }
  String get btnSelectorTime {
    return Intl.message('Time',
        name: 'btnSelectorTime',
        desc: 'Trial by time');
  }
  String get btnSelectorTries {
    return Intl.message('Tries',
        name: 'btnSelectorTries',
        desc: 'Trial by tries');
  }

  String get labelDataLoading {
    return Intl.message('Loading...',
        name: 'labelDataLoading',
        desc: 'wait for data loading');

  }



  String _labelStatTypeAndTime(datetime,type) => Intl.message("$datetime Timed: $type",
      name: '_labelStatTypeAndTime',
      desc: 'label for stat title',
      args: [datetime,type]
  );

  String labelStatTypeAndTime(int time,bool timed) {
    DateTime dt = DateTime.fromMillisecondsSinceEpoch(time);
    String timedString = timed ? lblYes : lblNo;
    String dateFormatted ="${DateFormat(DateFormat.YEAR_MONTH_DAY).format(dt)} ${DateFormat(DateFormat.HOUR24_MINUTE_SECOND).format(dt)}";
    return _labelStatTypeAndTime(dateFormatted, timedString);
  }

  String _labelStatData(success,time) => Intl.message("Solved $success in $time",
      name: '_labelStatData',
      desc: 'label for stat record subtitle',
      args: [success,time]
  );

  String formatSeconds(int seconds) {
    var fmt = NumberFormat("00");
    var mins =  (seconds/60).floor();
    var secs = (seconds % 60);
    return "${fmt.format(mins)}:${fmt.format(secs)}";
  }

  String formatTime(DateTime d) {
    DateFormat df = DateFormat("HH:mm:ss");
    return df.format(d);
  }

  String formatDateTime(DateTime d) {
    DateFormat df = DateFormat("dd-MM-yyyy HH:mm:ss");
    return df.format(d);
  }

  String formatDate(DateTime d) {
    DateFormat df = DateFormat("dd-MM-yyyy");
    return df.format(d);
  }

  String labelStatData(int success,int seconds) {
    DateTime dt = DateTime.fromMillisecondsSinceEpoch(seconds*1000);
    var fmt = NumberFormat("00");
    String strSuccess = success.toString(),strTime = "${fmt.format(dt.minute)}:${fmt.format(dt.second)}";
    return _labelStatData(strSuccess, strTime);
  }

  String messageSolutionCount(int count) => Intl.message(
      "You have solved $count expressions",
      name: 'messageSolutionCount',
      desc: 'Solution result (count)',
      args: [count]
  );

  String messageSolutionTime(String time) => Intl.message(
      "in $time",
      name: 'messageSolutionTime',
      desc: 'Solution result (time)',
      args: [time]
  );

  String get mathOperations => Intl.message("Operations",name: "mathOperations",desc: "Mathematical operations label");
  String get negativeValues => Intl.message("Negative values",name: "negativeValues",desc: "Allow negative values label");
  String get maxMemberValue => Intl.message("Max value",name: "maxMemberValue",desc: "Maximum member value in an expression");
  String get stopConditionLabel => Intl.message("Stop on: ",name: "stopConditionLabel",desc: "When to stop label");


  String stopConditionTries(int tries) => Intl.message("Tries: $tries",name: "stopConditionTries",desc: "format tries stop condition",args: [tries]);

  String _stopConditionTime(String timeValue) => Intl.message(
  "Time: $timeValue",
  name: "_stopConditionTime",desc: "format time stop condition",
  args: [timeValue]
  );

  String stopConditionTime(int seconds) {
    String value = formatSeconds(seconds);
    return _stopConditionTime(value);
  }

  String formatTriesOfTotal(int tries,int totalTries) => Intl.message("Tries: $tries of $totalTries",name: "formatTriesOfTotal",args:[tries,totalTries]);

  String get comingSoon => Intl.message("Coming soon...",name:"comingSoon",desc:"Coming soon banner");
  String get stopTraining => Intl.message("Stop and return",name:"stopTraining",desc:"Stop training and return to main page");
  String get pauseTraining => Intl.message("Pause",name:"pauseTraining",desc:"Pause training");
  String get resumeTraining => Intl.message("Resume",name:"resumeTraining",desc:"Resume training");

  String get appbarSessions => Intl.message("Sessions",name: 'appbarSessions',desc:"Sessions stats page title");
  String get appbarStatRecords => Intl.message("Records",name: 'appbarStatRecords',desc:"Stat records page title");
  String get appbarSessionTypes =>  Intl.message("Session types",desc:"Sessions stats page title");


  static final String paramsAdditionsTries = "additions_tries",paramsMultableTries="multable_tries",
      paramsAdditionsTime="additions_timed",paramsMultableTime="multable_timed",paramsAdditionsTriesSubzero="additions_tries_subzero";

  Map<String,String> parametersName = {
  paramsAdditionsTries : Intl.message("Additions and Subtractions by Tries",desc: "Settings name for + and - greate than zero"),
  paramsMultableTries : Intl.message("Multiplication table by Tries",desc: "Settings name for * greate than zero"),
  paramsAdditionsTime : Intl.message("Additions and Subtractions by Time",desc: "Settings name for + and - greate than zeros"),
  paramsMultableTime : Intl.message("Multiplication by Time",desc: "Settings name for * greate than zero"),
  paramsAdditionsTriesSubzero : Intl.message("Additions and Subtractions by Tries less than 0",desc: "Settings name for + and - less than zeros")
  };

  String get btnLabelSettings => Intl.message("Settings",name:"btnLabelSettings",desc: "label for settings button");
  String get btnLabelBack => Intl.message("Back",name:"btnLabelBack",desc: "label for settings button");

}
