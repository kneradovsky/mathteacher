import 'package:flutter/material.dart';

typedef KeyboardCallback(int key);

class NumericalKeyboard extends StatelessWidget {
  double totalHeight;
  double _rowHeight;
  NumericalKeyboard({Key key, this.onKeyPressed, this.totalHeight=double.infinity}) : super(key: key);

  final double _desiredRowHeight = 70;

  static const backspaceKey = 42;
  static const clearKey = 69;
  static const equalsKey = 77;
  static const changeSignKey = 12;


  final KeyboardCallback onKeyPressed;
  BuildContext _ctx;
  TextStyle labelKeyStyle;

  @override
  Widget build(BuildContext context) {
    labelKeyStyle = Theme.of(context).textTheme.headline.merge(TextStyle(inherit: true,color: Theme.of(context).colorScheme.onPrimary));
    _ctx = context;
    _rowHeight = _desiredRowHeight;
    if(totalHeight!=double.infinity) {
      _rowHeight = totalHeight/4;
    }
    if(_rowHeight>_desiredRowHeight) _rowHeight = _desiredRowHeight;
    return Material(
      child: Table(
        defaultColumnWidth: IntrinsicColumnWidth(flex: 1.0),
        children: [
          TableRow(
            children: [
              _buildNumberKey(7),
              _buildNumberKey(8),
              _buildNumberKey(9),
            ],
          ),
          TableRow(
            children: [
              _buildNumberKey(4),
              _buildNumberKey(5),
              _buildNumberKey(6),
            ],
          ),
          TableRow(
            children: [
              _buildNumberKey(1),
              _buildNumberKey(2),
              _buildNumberKey(3),
            ],
          ),
          TableRow(
            children: [
              _buildKey(Text("+/-",style: labelKeyStyle), changeSignKey),
              _buildNumberKey(0),
              _buildKey(Container(child:Icon(Icons.backspace,color: Theme.of(context).colorScheme.onPrimary),padding: EdgeInsets.only(top:2,bottom:2),), backspaceKey),
            ],
          )
        ],
      ),
    );
  }

  Widget _buildNumberKey(int n) {
    return _buildKey(Text('$n',style: labelKeyStyle), n);
  }

  Widget _buildKey(Widget icon, int key) {
    return Container(
        height: _rowHeight,
      padding: EdgeInsets.all(2.0) ,
      child: RaisedButton(
          key: Key('numkey$key'),
          child: icon,
          color: Theme.of(_ctx).primaryColor,
          padding: EdgeInsets.all(0),
        onPressed: () => onKeyPressed(key),
      )
    );

  }
}