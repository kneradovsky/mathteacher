import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

enum CountdownTimerCommands {
 Pause,Resume,Reset,Stop
}

class CountdownTimer extends StatefulWidget {

  int initialSeconds;
  Function onExpired;
  double width,height;
  Stream<CountdownTimerCommands> controlStream;

  CountdownTimer({Key key, this.initialSeconds,this.onExpired,this.height,this.width,this.controlStream}) : super(key:key);



  @override
  _CountdownTimerState createState() => _CountdownTimerState();
}

class _CountdownTimerState extends State<CountdownTimer> {
  int secondsLeft;
  bool isPaused=false;

  Timer timer;

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(Duration(seconds: 1), _timerTick);
    secondsLeft = widget.initialSeconds;
    if(widget.controlStream!=null) widget.controlStream.listen(onCommand);
  }

  @override
  void dispose() {
    if(timer!=null) timer.cancel();
    super.dispose();
  }

  void onCommand(CountdownTimerCommands cmd) {
    if(cmd==CountdownTimerCommands.Pause) setState(() {isPaused=true;});
    if(cmd==CountdownTimerCommands.Resume) setState(() {isPaused=false;});
    if(cmd==CountdownTimerCommands.Stop) {
      setState((){isPaused = true;});
      if (widget.onExpired != null) widget.onExpired((widget.initialSeconds-secondsLeft).abs());
    }

  }

  @override
  Widget build(BuildContext context) {
    TextStyle timeLabel = Theme.of(context).textTheme.headline.merge(TextStyle(
      inherit: true,
      color: Theme.of(context).accentColor,
      fontFamily: "RobotoMono",
    ));
    return Stack(
      children: <Widget>[
        Container(
          height: widget.height,
          width: widget.width,
          child: CircularProgressIndicator(
            strokeWidth: 8,
            //backgroundColor: Colors.blue[900],
            //valueColor: AlwaysStoppedAnimation<Color>(Theme.of(context).backgroundColor),
            value: getProgressValue(),
          ),
        ),
        Container(
            width: widget.width,
            height: widget.height,
            alignment: Alignment.center,
            child: Text(
              formatTime(secondsLeft),
              style: timeLabel,
            ))
      ],
    );
  }

  void _timerTick(Timer t) {
    if(isPaused) return;
    if (secondsLeft == 0 && widget.initialSeconds!=0) {
      if (widget.onExpired != null) widget.onExpired((widget.initialSeconds - secondsLeft).abs());
      isPaused=true;
      return;
    }
    setState(() {
      if(widget.initialSeconds==0)
        secondsLeft = secondsLeft + 1;
      else secondsLeft = secondsLeft - 1;
    });
  }

  double getProgressValue() {
    if(widget.initialSeconds!=0) return secondsLeft/widget.initialSeconds;
    int oddMinute = secondsLeft~/60 % 2;
    int modulus = secondsLeft % 60;
    return (oddMinute>0 ? 60-modulus : modulus)/60;
  }

  String formatTime(int seconds) {
    var format = NumberFormat("00");
    int minutes = (seconds / 60).floor();
    int secs = seconds % 60;
    return format.format(minutes) + ":" + format.format(secs);
  }

}
