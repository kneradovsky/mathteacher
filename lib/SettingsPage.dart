import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mathteacher/expressions.dart';
import 'package:mathteacher/togglebutton.dart';

import 'package:mathteacher/LocalMessages.dart';
import 'package:mathteacher/RoundedButton.dart';
import 'package:mathteacher/core/Settings.dart';

class SettingsPage extends StatefulWidget {
  String operation;

  SettingsPage({Key key, this.operation}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  static final String operADD = "+",
      operSUB = "-",
      operMUL = "*",
      operDIV = "/";
  static final Map<String, String> operIcons = {
    operADD: "assets/icons/plus-med.png",
    operSUB: "assets/icons/minus-med.png",
    operMUL: "assets/icons/multiply-med.png",
    operDIV: "assets/icons/division-med.png",
  };

  ExpressionGenerator generator = ExpressionGenerator();
  Settings settings = Settings();
  bool testPred1 = false, testPred2 = false;
  TextStyle bigLabel, midLabel, smallLabel, buttonLabel;
  CountThisLocalizations localStrings;

  @override
  void didChangeDependencies() {
    localStrings = CountThisLocalizations.of(context);
    bigLabel = Theme.of(context).textTheme.display1.merge(TextStyle(
          inherit: true,
          color: Colors.black,
          fontFamily: "Rubik",
        ));
    midLabel = Theme.of(context).textTheme.headline.merge(TextStyle(
          inherit: true,
          color: Colors.black,
          fontFamily: "Rubik",
        ));
    smallLabel = Theme.of(context).textTheme.title.merge(TextStyle(
          inherit: true,
          color: Colors.black,
          fontWeight: FontWeight.normal,
          fontFamily: "Rubik",
        ));
    buttonLabel = Theme.of(context).textTheme.display2.merge(TextStyle(
          inherit: true,
          color: Color(0xFFDCE8BA),
          fontFamily: "Rubik",
        ));
    super.didChangeDependencies();
  }

  String get conditionText {
    if (settings.isTimed) {
      return localStrings.stopConditionTime(settings.seconds);
    } else {
      return localStrings.stopConditionTries(settings.numTries);
    }
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    return Material(
      child: Container(
          decoration: BoxDecoration(color: Theme.of(context).backgroundColor),
          child: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                    padding: EdgeInsets.only(top: 20, bottom: 20),
                    child: Center(
                      child: Text(localStrings.mathOperations, style: bigLabel),
                    )),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <String>[operADD, operSUB, operMUL, operDIV]
                        .map((oper) => ToggleButton(
                            label: oper,
                            handlerOn: () => operHandler(oper, false),
                            handlerOff: () => operHandler(oper, true),
                            predicate: () =>
                                settings.operations.contains(oper),
                            child: Image.asset(operIcons[oper]),
                            size: 64))
                        .toList()),
                Container(
                  padding:
                      EdgeInsets.only(top: 30, bottom: 10, left: 20, right: 10),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        localStrings.negativeValues,
                        style: midLabel,
                      ),
                      Material(
                        color: Theme.of(context).backgroundColor,
                        child: Switch(
                          activeColor: theme.primaryColor,
                          activeTrackColor: theme.primaryColorDark,
                          inactiveTrackColor:
                              theme.colorScheme.secondaryVariant,
                          inactiveThumbColor: theme.colorScheme.secondary,
                          value: settings.isSubZero,
                          onChanged: (val) => setState(() {
                            //settings.isSubZero = val;
                          }),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(localStrings.maxMemberValue, style: midLabel),
                        Text(
                          settings.maxNumber.toString(),
                          style: midLabel,
                        )
                      ],
                    )),
                Slider(
                  value: settings.maxNumber.toDouble(),
                  min: 10,
                  max: 100,
                  divisions: 9,
                  label: settings.maxNumber.toString(),
                  onChanged: (change) => setState(() {
                    //settings.maxNumber = change.toInt();
                  }),
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        child: Text(localStrings.stopConditionLabel,
                            style: midLabel),
                      ),
                      createSliderButtons(),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(20),
                  child: Text(conditionText, style: midLabel),
                ),
                Expanded(
                    child: Container(
                  padding: EdgeInsets.only(bottom: 20),
                  child: Container(
                    alignment: Alignment.bottomCenter,
                    height: 100,
                    child: RoundedShadowButton(
                      key: Key("okButton"),
                      baseColor: Theme.of(context).accentColor,
                      shadowColor: Theme.of(context).primaryColorDark,
                      child: Text(localStrings.lblOK, style: buttonLabel),
                      onPressed: () => Navigator.of(context).pop(),
                    ),
                  ),
                )),
              ],
            ),
          )),
    );
  }

  void operHandler(String oper, bool add) {

  }

  Widget createSliderButtons() {
    var textStyleOnBg = smallLabel;
    var textStyleOnPrimary = smallLabel.merge(TextStyle(
        inherit: true, color: Theme.of(context).colorScheme.onPrimary));
    double chipWidth = 100;
    double chipHeight = 42;
    var locStrings = CountThisLocalizations.of(context);
    return SizedBox(
      height: chipHeight,
      width: chipWidth * 2,
      child: Container(
        decoration: ShapeDecoration(
            shape: RoundedRectangleBorder(
                side: BorderSide(
                    width: 1.0,
                    color: Theme.of(context).colorScheme.primaryVariant),
                borderRadius: BorderRadius.circular(5.0))),
        child: GestureDetector(
          key: Key('optionTimed'),
          onTap: () => setState(() {
            //settings.isTimed = !settings.isTimed;
          }),
          child: Stack(
            children: <Widget>[
              AnimatedPositioned(
                duration: Duration(milliseconds: 200),
                left: settings.isTimed ? 0 : chipWidth,
                top: -1,
                child: Container(
                    height: chipHeight,
                    width: chipWidth,
                    decoration: ShapeDecoration(
                      color: Theme.of(context).colorScheme.primary,
                      shape: RoundedRectangleBorder(
                          side: BorderSide(style: BorderStyle.none, width: 1.0),
                          borderRadius: BorderRadius.circular(5)),
                    )),
              ),
              Positioned(
                left: 0,
                top: 0,
                child: SizedBox(
                    height: chipHeight,
                    width: chipWidth,
                    child: Container(
                        padding: EdgeInsets.only(left: 0.0),
                        alignment: Alignment.center,
                        child: Text(locStrings.btnSelectorTime,
                            style: settings.isTimed
                                ? textStyleOnPrimary
                                : textStyleOnBg))),
              ),
              Positioned(
                left: chipWidth,
                top: 0,
                child: SizedBox(
                    height: chipHeight,
                    width: chipWidth,
                    child: Container(
                        padding: EdgeInsets.only(right: 0.0),
                        alignment: Alignment.center,
                        child: Text(locStrings.btnSelectorTries,
                            style: settings.isTimed
                                ? textStyleOnBg
                                : textStyleOnPrimary))),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
