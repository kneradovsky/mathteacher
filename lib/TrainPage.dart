import 'dart:async';
import 'dart:math' as math;
import 'package:mathteacher/CountDownTimer.dart';
import 'package:mathteacher/LocalMessages.dart';
import 'package:mathteacher/model/model.dart';
import 'package:mathteacher/numkeyboard.dart';

import 'package:flutter/material.dart';
import 'package:mathteacher/expressions.dart';
import 'package:mathteacher/RoundedButton.dart';
import 'package:mathteacher/core/Settings.dart';
import 'package:mathteacher/Routes.dart';
import 'package:mathteacher/StackedPile.dart';

class TrainPage extends StatefulWidget {
  @override
  _TrainPageState createState() => _TrainPageState();
}

class _TrainPageState extends State<TrainPage> with WidgetsBindingObserver, SingleTickerProviderStateMixin {
  AppLifecycleState appstate;

  static const String initialResult = "??";
  String result = initialResult;

  AnimationController _controller;
  int successCount = 0;
  int errorCount = 0;
  bool isPaused = false;
  ExpressionGenerator generator = ExpressionGenerator();
  MathExpression currentExp;


  final int initialSeconds = 60;
  TextStyle pileLabel, expLabel,labelKeyStyle;

  StreamController cd_timer = StreamController<CountdownTimerCommands>();
  DateTime dtStart,curExpStart;
  CountThisLocalizations localStrings;
  Settings settings = Settings();
  int sesid;

  @override
  void initState() {
    _controller = AnimationController(vsync: this);
    currentExp = generator.expression;
    dtStart = DateTime.now();
    curExpStart = DateTime.now();
    Session(start: dtStart,duration: 0,title: Settings().activeName).save().then( (sesid) => setState(() {this.sesid=sesid;}));
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }


  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    setState(() {appstate = state;});
  }

  @override
  void didChangeDependencies() {
    localStrings = CountThisLocalizations.of(context);
    super.didChangeDependencies();
  }




  @override
  Widget build(BuildContext context) {
    pileLabel = Theme.of(context).textTheme.display2.merge(TextStyle(
          inherit: true,
          color: Color(0xFFDCE8BA),
          fontFamily: "Rubik",
        ));
    expLabel = Theme.of(context).textTheme.display2.merge(TextStyle(
          inherit: true,
          color: Color(0xFFFFFFFF),
          fontFamily: "Rubik",
        ));

    labelKeyStyle = Theme.of(context).textTheme.headline.merge(TextStyle(inherit: true,color: Theme.of(context).colorScheme.onPrimary));
    var buttonLabel = Theme.of(context).textTheme.subtitle.merge(TextStyle(
      inherit: true,
      color: Color(0xFFFFFFFF),
      fontFamily: "Rubik",
    ));
    
    var transformation = Matrix4.identity();
    if(appstate==AppLifecycleState.inactive && !isPaused) pauseTraining();
    if(appstate==AppLifecycleState.resumed && isPaused) pauseTraining();
    return Material(child:Container(
        decoration: BoxDecoration(color: Theme.of(context).backgroundColor),
        child: SafeArea(
            child: Column(
              mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 2),
                  child: FlatButton(
                    key: Key("stopTraining"),
                    //color: Theme.of(context).accentColor,
                    child: Icon(Icons.keyboard_return,size: 36,),
                    onPressed: stopTraining,
                  ),
                ),
                Container(
                  child: Text(Settings().isTimed ? (successCount+errorCount).toString() : localStrings.formatTriesOfTotal((successCount+errorCount), Settings().numTries))
                ),
                Container(
                  padding: EdgeInsets.only(right: 2),
                  child: FlatButton(
                    key: Key("pauseTraining"),
                    //color: Theme.of(context).accentColor,
                      child: Icon(isPaused ? Icons.play_arrow : Icons.pause,size: 36,),
                  onPressed: pauseTraining,),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                StackedPile(
                  color: Theme.of(context).primaryColorDark,
                  initialCount: successCount,
                  size: Size(70, 100),
                  child:
                      Center(child: Text(successCount.toString(), style: pileLabel)),
                ),
                CountdownTimer(
                  initialSeconds: settings.isTimed ? settings.seconds : 0,
                  width: 90,
                  height: 90,
                  onExpired: _timerExpired,
                  controlStream: cd_timer.stream,
                ),
                StackedPile(
                  color: Theme.of(context).errorColor,
                  initialCount: errorCount,
                  size: Size(70, 100),
                  child:
                      Center(child: Text(errorCount.toString(), style: pileLabel)),
                ),
              ],
            ),
            Container(padding: EdgeInsets.only(top:5),),
            Container(
              padding: EdgeInsets.only(left: 20,right: 20),
              child: Container(
                height: 200,
                decoration: ShapeDecoration(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15)),
                    color: Theme.of(context).primaryColor),
                alignment: Alignment.center,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Center(
                        child: Text(

                      currentExp.toString(),
                          key: Key("expression"),

                          style: expLabel,
                    )),
                    RotatedBox(
                      quarterTurns: 1,
                      child: Text(
                        "=",
                        style: expLabel,
                      ),
                    ),
                    Center(

                        child: Text(
                      result,
                          key: Key("result"),

                          style: expLabel,
                    ))
                  ],
                ),
              ),
            ),
            Container(padding: EdgeInsets.only(top:10),),
            Expanded(child: createKeyboard()),
          ],
        ))));
  }

  Widget createKeyboard() {
    return LayoutBuilder(
      builder: (BuildContext context,BoxConstraints constraints) {
        double keyboardHeight = constraints.maxHeight/5*4;
        double equalButtonHeight = constraints.maxHeight/5;
        equalButtonHeight = equalButtonHeight > 70 ? 70 : equalButtonHeight;
        return Column(
          children: <Widget>[
            NumericalKeyboard(
              totalHeight: keyboardHeight,
              onKeyPressed: _onNumerickeyPressed,

            ),
            Container(
              height: equalButtonHeight,
              padding: EdgeInsets.all(2),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Expanded(
                    child: RaisedButton(
                      key: Key("equalsButton"),
                      color: Theme.of(context).colorScheme.primary,
                      child: Text("=",style: labelKeyStyle,),
                      onPressed: checkResult,
                    ),
                  ),
                ],
              ),
            )

          ],
        );
      },
    );
  }

  void _onNumerickeyPressed(int key) {
      bool isZeros = RegExp(r"^0+$").hasMatch(result);
      setState(() {
        if (key < 10) {
          if (result == initialResult)
            result = key.toString();
          else
            result += key.toString();
        } else if (key == NumericalKeyboard.backspaceKey && result != initialResult) {
          result = result.substring(0, result.length - 1);
          if (result.length == 0) result = initialResult;
        } else if (key == NumericalKeyboard.changeSignKey &&
            result.length > 0 &&
            result != initialResult && !isZeros) {
          var res = result;
          if (res.startsWith("-"))
            result = res.substring(1);
          else
            result = "-" + res;
        }
      });
  }

  void checkResult() {
    if(isPaused || result==initialResult) return;
    num res = currentExp.answer;
    String correct = res.toInt().toString();
    saveStats();
    if (correct == result) {
      successCount++;
    } else errorCount++;
    currentExp = generator.expression;
    result = initialResult;
    setState(() {
    });
    curExpStart = DateTime.now();
    if(!settings.isTimed && (successCount+errorCount)>=settings.numTries) cd_timer.sink.add(CountdownTimerCommands.Stop);
  }

  void pauseTraining() {
    isPaused = !isPaused;
    if(isPaused)
      cd_timer.add(CountdownTimerCommands.Pause);
    else cd_timer.add(CountdownTimerCommands.Resume);
    setState((){isPaused;});
  }


  void _timerExpired(int elapsedSeconds) {
    saveSession();
    var okLabel = Theme.of(context).textTheme.subhead.merge(TextStyle(
      inherit: true,
      color: Color(0xFFFFFFFF),
      fontWeight: FontWeight.bold
    ));
    var textStyle = Theme.of(context).textTheme.subhead.merge(TextStyle(inherit: true,color: Theme.of(context).colorScheme.onBackground));
    showDialog(
      barrierDismissible: false,
        context: context,
        builder: (context) {
              return Dialog(
                backgroundColor: Colors.transparent,
                      child: IntrinsicWidth(
                          child: Container(
                            padding: EdgeInsets.all(20),
                            decoration: BoxDecoration(
                                border: Border.all(color: Theme.of(context).colorScheme.background),
                                color: Theme.of(context).colorScheme.background,
                                borderRadius: BorderRadius.all(Radius.circular(10))),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(bottom: 10),
                                    alignment: Alignment.center,
                                    child: Text(localStrings.messageSolutionCount(successCount),style: textStyle,)),
                                Container(
                                  padding: EdgeInsets.only(bottom: 10),
                                    child: Text(localStrings.messageSolutionTime(localStrings.formatSeconds(elapsedSeconds)),style: textStyle,)),
                                RoundedShadowButton(
                                  width: 150,
                                  height: 50,
                                  baseColor: Theme.of(context).accentColor,
                                  shadowColor: Theme.of(context).primaryColorDark,
                                  child: Text(MaterialLocalizations.of(context).okButtonLabel,style:okLabel),
                                  onPressed: () => Navigator.of(context).popUntil(ModalRoute.withName(homeRoute)),
                                )

                              ],
                            ),
                          ),
                        ),
                  );
        });

  }

  void saveSession() async {
    var ses = await Session().getById(sesid);
    ses.duration = DateTime.now().millisecondsSinceEpoch - dtStart.millisecondsSinceEpoch;
    ses.correct=successCount;
    ses.errors=errorCount;
    await ses.save();
  }
  void stopTraining() async {
    await saveSession();
    Navigator.of(context).pop();

  }

  void saveStats() {
    int duration = DateTime.now().millisecondsSinceEpoch - curExpStart.millisecondsSinceEpoch;
    int solution = currentExp.answer.toInt();
    int answer = int.tryParse(result) ?? 0;
    Statrecord
        .withFields(curExpStart, duration, currentExp.toString(), solution.toDouble(), answer.toDouble(), answer == solution, sesid)
        .save();
  }
}
