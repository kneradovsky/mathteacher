import 'package:flutter/material.dart';
import 'package:mathteacher/LocalMessages.dart';


class ComingSoon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var locstr  = CountThisLocalizations.of(context);
    return Scaffold(
      appBar: AppBar(title: Text(locstr.comingSoon),),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              alignment: Alignment.center,
              child: Text(locstr.comingSoon,style: Theme.of(context).textTheme.display1,),
            )
          )
        ],
      )
    );
  }
}
