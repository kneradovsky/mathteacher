import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:mathteacher/LocalMessages.dart';
import 'package:mathteacher/Routes.dart';
import 'package:mathteacher/StartPage.dart';
import 'package:mathteacher/core/Settings.dart';

import 'package:mathteacher/theme.dart';


void main() {
  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Settings();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      onGenerateTitle: (context) => CountThisLocalizations.of(context).appTitle,
      theme: myGlobalTheme,
      home: Container(
        decoration: BoxDecoration(color: myGlobalTheme.backgroundColor),
          child: SafeArea(child: StartPage())),
      initialRoute: homeRoute,
      onGenerateRoute: onGenerateCountRoute,
      supportedLocales: [
        Locale('en'),
        Locale('ru')
      ],
      localizationsDelegates: [
        CountThisLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate
      ],
    );
  }
}

