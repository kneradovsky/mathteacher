import 'dart:convert';
import 'dart:typed_data';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:sqfentity/sqfentity.dart';
import 'package:sqfentity_gen/sqfentity_gen.dart';

part 'model.g.dart';

const tableSession = SqfEntityTable(
    tableName: "session",
    primaryKeyName: 'id',
    primaryKeyType: PrimaryKeyType.integer_auto_incremental,
    useSoftDeleting: false,
    modelName: null,
    fields: [
      SqfEntityField('start',DbType.datetime),
      SqfEntityField('duration',DbType.integer),
      SqfEntityField('title',DbType.text,defaultValue: ""),
      SqfEntityField('correct',DbType.integer,defaultValue: 0),
      SqfEntityField('errors',DbType.integer,defaultValue: 0)
    ]
);

const tableStatRecord = SqfEntityTable(
  tableName: 'statrecords',
  primaryKeyName: 'id',
  primaryKeyType: PrimaryKeyType.integer_auto_incremental,
  useSoftDeleting: false,
  modelName: null,
  fields: [
    SqfEntityField('timestamp',DbType.datetime),
    SqfEntityField('duration',DbType.integer),
    SqfEntityField('expression',DbType.text),
    SqfEntityField('solution',DbType.real),
    SqfEntityField('answer',DbType.real),
    SqfEntityField('correct',DbType.bool),
      SqfEntityFieldRelationship(
          parentTable: tableSession,
          deleteRule: DeleteRule.CASCADE,
          defaultValue: 0
      )
  ]
);

const seqIdentity = SqfEntitySequence(
    sequenceName: 'identity'
);

@SqfEntityBuilder(dbModel,
)
const dbModel = SqfEntityModel(
  modelName: 'myDb',
  databaseName: 'statdatabase.db',
  databaseTables: [tableStatRecord,tableSession],
  sequences: [seqIdentity],
  bundledDatabasePath: null
);

