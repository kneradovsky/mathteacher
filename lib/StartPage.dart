import 'package:flutter/material.dart';
import 'package:mathteacher/LocalMessages.dart';
import 'package:mathteacher/Routes.dart';
import 'package:mathteacher/theme.dart';
import 'RoundedButton.dart';

class StartPage extends StatefulWidget {
  @override
  _StartPageState createState() => _StartPageState();
}

class _StartPageState extends State<StartPage> {
  CountThisLocalizations _localStrings;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _localStrings = CountThisLocalizations.of(context);
  }

  @override
  Widget build(BuildContext context) {
    TextStyle headingStyle = Theme.of(context).textTheme.display2.merge(
        TextStyle(
            inherit: true,
            color: Color(0xFFF46060),
          fontFamily: "Rubik",
        )
    );
    TextStyle buttonLabel = Theme.of(context).textTheme.display2.merge(
        TextStyle(
          inherit: true,
          color: Color(0xFFDCE8BA),
          fontFamily: "Rubik",
        )
    );
    var textTheme = MathTextTheme(context);
    var bigLabel = textTheme.bigLabel.merge(TextStyle(inherit: true,color: Color(0xFFDCE8BA)));
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Spacer(flex: 1,),
          Expanded(flex: 10,child:
          Column(
            children: <Widget>[
              Container(
                alignment: Alignment.topCenter,
                child: Text(_localStrings.appTitle,style: headingStyle,),
              ),
              Container(padding: EdgeInsets.all(10),),
              RoundedShadowButton(
                key: Key("trainingPage"),
                baseColor: Color(0xFF50CB86),
                shadowColor: Color(0xFF0A6733),
                child: Text(_localStrings.btnTrialStart,style:buttonLabel),
                onPressed: () => Navigator.of(context).pushNamed(trainRoute),
              ),
              Container(padding: EdgeInsets.all(10),),
              RoundedShadowButton(
                key: Key("settingsPage"),
                baseColor: Color(0xFFFC993C),
                shadowColor: Color(0xFF975404),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Icon(Icons.settings,size: 50,color: Color(0xFFDCE8BA)),
                    Text(_localStrings.btnLabelSettings,style:bigLabel)
                  ],
                ),
                onPressed: () => Navigator.of(context).pushNamed(setOpersRoute),
              ),
              Container(padding: EdgeInsets.all(10),),
              RoundedShadowButton(
                key: Key("settingsPage2"),
                baseColor: Color(0xFFFC993C),
                shadowColor: Color(0xFF975404),
                child: Text(_localStrings.comingSoon,style:bigLabel),
                onPressed: () => Navigator.of(context).pushNamed(comingSoonRoute),
              ),
              Container(padding: EdgeInsets.all(10),),
              RoundedShadowButton(
                key: Key("statsPage"),
                baseColor: Color(0xFFFC993C),
                shadowColor: Color(0xFF975404),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Image.asset("assets/icons/chart-bar-med.png",color: Color(0xFFDCE8BA),),
                    Image.asset("assets/icons/chart-areaspline-med.png",color: Color(0xFFDCE8BA)),
                  ],
                ),
                onPressed: () => Navigator.of(context).pushNamed(statsRoute),
              ),

            ],
          )),
        ],
      ),
    );
  }
}



