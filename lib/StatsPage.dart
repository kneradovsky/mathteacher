import 'dart:math';

import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/rendering.dart';
import 'package:mathteacher/LocalMessages.dart';
import 'package:mathteacher/Routes.dart';
import 'package:mathteacher/core/Settings.dart';
import 'package:mathteacher/model/model.dart';
import 'package:mathteacher/theme.dart';

class StatsPage extends StatefulWidget {
  @override
  _StatsPageState createState() => _StatsPageState();
}

class _StatsPageState extends State<StatsPage> {
  CountThisLocalizations localStrings;

  @override
  void didChangeDependencies() {
    localStrings = CountThisLocalizations.of(context);
    super.didChangeDependencies();
    //set style for arcs
    var style = ChartsMaterialStyle(Theme.of(context));
    charts.StyleFactory.style = style;

  }

  int totalCorrect = 0, totalErrors = 0;

  List<Session> sessions = List();

  @override
  Widget build(BuildContext context) {
    var textTheme = MathTextTheme(context);
    var theme = Theme.of(context);
    var locStr = CountThisLocalizations.of(context);
    return Container(
        decoration: BoxDecoration(color: Theme.of(context).backgroundColor),
        child: Scaffold(
            appBar: AppBar(title: Text(locStr.appbarSessions)),
            body: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                      child: SingleChildScrollView(child: createSessionsDatatable())),
                  Expanded(
                      child: Container(
                          decoration: BoxDecoration(
                              color: theme.colorScheme.background),
                          child: createSessionChart()))
                ])));
  }

  Widget createSessionsDatatable() {
    var textTheme = MathTextTheme(context);
    var theme = Theme.of(context);
    var textStyle = textTheme.body1;

    var smallOnError = textStyle.merge(TextStyle(color:theme.colorScheme.onError));
    var locStr = CountThisLocalizations.of(context);

    var onSesTap = (ses) => () => Navigator.of(context).pushNamed(statrecsRoute, arguments: ses);

    return Container(
      decoration: BoxDecoration(color: theme.colorScheme.background),
      child: DataTable(
        columns: [
          DataColumn(label: Icon(Icons.access_time)),
          DataColumn(label: Icon(Icons.fingerprint)),
          DataColumn(label: Icon(Icons.timer)),
          DataColumn(label: Icon(Icons.check))
        ],
        rows: sessions.map( (ses) => DataRow(
          cells: [
            DataCell(Text(locStr.formatDateTime(ses.start)),onTap: onSesTap(ses)),
            DataCell(getTitleWidget(ses),onTap: onSesTap(ses)),
            DataCell(Text(locStr.formatSeconds((ses.duration/1000).truncate())),onTap: onSesTap(ses)),
            DataCell(GreenRedArcChart(red:ses.errors,green:ses.correct,arcwidth: 3,),onTap: onSesTap(ses))
          ]
        )).toList()
      ),
    );
  }

  Widget getTitleWidget(Session ses) {
    var locStr = CountThisLocalizations.of(context);
    var params = Settings().getByName(ses.title) ?? Settings().active;
    return Column(
      children: <Widget>[
        Container(alignment: Alignment.centerLeft,child: Text(params.operations.join(" "),)),
        Row(
        children: <Widget>[
          Icon(params.isTimed ? Icons.timer : Icons.autorenew,size: 12),
          Text(params.isTimed ? locStr.formatSeconds(params.seconds) : params.numTries.toString())
        ],
      ),
      ]
    );
  }

  Widget createSessionChart() {
    var theme = Theme.of(context);
    var redColor = theme.colorScheme.error;
    final List<charts.Series<GaugeSegment, String>> series = [
      charts.Series<GaugeSegment, String>(
          id: "Sessions",
          domainFn: (segment, _) => segment.name,
          measureFn: (segment, _) => segment.size,
          labelAccessorFn: (segment, _) => segment.name,
          colorFn: (segment, _) => segment.chartsColor,
          data: [
            GaugeSegment(
                "+",
                totalCorrect,
                theme.accentColor),
            GaugeSegment(
                "-",
                totalErrors,
                redColor),
          ])
    ];

    return charts.PieChart(series,
        animate: true,
        defaultRenderer: charts.ArcRendererConfig(
          arcWidth: 50,
          startAngle: 4 / 5 * pi,
          arcLength: 7 / 5 * pi,
          arcRendererDecorators: [charts.ArcLabelDecorator()],
        ));
  }

  @override
  void initState() {
    Session().select().orderByDesc(['start']).toList()
        .then((sessions) => setState(() {this.sessions = sessions;}));
    Session().select(columnsToSelect: [
          SessionFields.correct.sum("correct"),
          SessionFields.errors.sum("errors"),
        ]).toSingle().then((ses) => setState(() {
              totalErrors = ses.errors;
              totalCorrect = ses.correct;
        }));
  }

}



class ChartsMaterialStyle extends charts.MaterialStyle {
  final ThemeData theme;
  const ChartsMaterialStyle(this.theme);

  @override
  charts.Color get arcStrokeColor {
    var c = theme.colorScheme.background;
    charts.Color(a:c.alpha,r:c.red,g:c.green,b:c.blue);
  }


}

class GaugeSegment {
  final String name;
  final int size;
  final Color color;
  charts.Color get chartsColor {
    return charts.Color(r:color.red,g:color.green,b:color.blue,a:color.alpha);
  }
  GaugeSegment(this.name, this.size, this.color);
}

class StatsRecordsPage extends StatefulWidget {
  Object args;

  StatsRecordsPage({this.args}) : super();

  @override
  _StatsRecordsPageState createState() => _StatsRecordsPageState();
}

class _StatsRecordsPageState extends State<StatsRecordsPage> {
  Session ses;
  List<Statrecord> recs = List();
  int totalRecs = 0;
  int successCount = 0;

  @override
  void initState() {
    ses = widget.args as Session;
    Statrecord().select().sessionId.equals(ses.id).toList()
        .then((inrecs) {
          recs = inrecs;
          totalRecs = recs.length;
          successCount = recs.where( (sr) => sr.correct).length;
          setState((){});
        });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var locStr = CountThisLocalizations.of(context);
    return Scaffold(
        backgroundColor: theme.colorScheme.background,
        appBar: AppBar(title: Text(locStr.appbarStatRecords)),
        body: Container(
          child: Column(
            children: <Widget>[
              Expanded(
                child: SingleChildScrollView(
                  child: createDataTable()
                )
              ),
              Expanded(
                child: Container(decoration: BoxDecoration(color: theme.colorScheme.background),
                child: createCharts())
              )
            ]
          ),
        ));
  }

  Widget createDataTable() {
    var textTheme = MathTextTheme(context);
    var theme = Theme.of(context);
    var textStyle = textTheme.body1;

    var smallOnError = textStyle.merge(TextStyle(color:theme.colorScheme.onError));
    var locStr = CountThisLocalizations.of(context);
    return DataTable(
      columnSpacing: 15.0,
      dataRowHeight: 23.0,
      headingRowHeight: 35.0,
      columns: [
        DataColumn(label: Icon(Icons.access_time)),
        DataColumn(label: Icon(Icons.timer)),
        DataColumn(label: Text("???",style: textTheme.smallLabel)),
        DataColumn(label: Icon(Icons.check))
      ],
      rows: recs.map( (r) => DataRow(
        cells: [
          DataCell(Text(locStr.formatTime(r.timestamp)),),
          DataCell(Text(locStr.formatSeconds((r.duration/1000).toInt()),style: textStyle,),),
          DataCell(Text(r.expression,style: textStyle,),),
          DataCell(
            Container(
              padding: EdgeInsets.all(1.0),
              child: Container(
                  decoration: r.correct ? null : BoxDecoration(border: Border.all(width: 1.0,color: theme.colorScheme.error),borderRadius: BorderRadius.circular(2.0),color: theme.colorScheme.error,),
                  width: 25,
                  height: 25,
                  alignment: Alignment.center,
                  child: Text(r.answer.truncate().toString(),style: r.correct ? textStyle : smallOnError,)),
            ),),
        ]
      )).toList(),
    );
  }

  Widget createCharts() {
    var theme = Theme.of(context);
    var redColor = theme.colorScheme.error;
    final List<charts.Series<GaugeSegment, String>> series = [
      charts.Series<GaugeSegment, String>(
          id: "Sessions",
          domainFn: (segment, _) => segment.name,
          measureFn: (segment, _) => segment.size,
          labelAccessorFn: (segment, _) => segment.name,
          colorFn: (segment, _) => segment.chartsColor,
          data: [
            GaugeSegment(
                "+",
                successCount,
                theme.accentColor),
            GaugeSegment(
                "-",
                totalRecs-successCount,
                redColor),
          ])
    ];

    return charts.PieChart(series,
        animate: true,
        defaultRenderer: charts.ArcRendererConfig(
          arcWidth: 50,
          startAngle: 4 / 5 * pi,
          arcLength: 7 / 5 * pi,
          arcRendererDecorators: [charts.ArcLabelDecorator()],
        ));

  }
}

class GreenRedArcChart extends StatelessWidget {
  int green=0,red=0;
  int arcwidth = 5;
  bool animate = false;
  GreenRedArcChart({this.red,this.green,this.arcwidth,this.animate});

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var redColor = theme.colorScheme.error;
    final List<charts.Series<GaugeSegment, String>> series = [
      charts.Series<GaugeSegment, String>(
          id: "Sessions",
          domainFn: (segment, _) => segment.name,
          measureFn: (segment, _) => segment.size,
          labelAccessorFn: (segment, _) => segment.name,
          colorFn: (segment, _) => segment.chartsColor,
          data: [
            GaugeSegment(
                "+",
                green,
                theme.accentColor),
            GaugeSegment(
                "-",
                red,
                redColor),
          ])
    ];
    return charts.PieChart(series,
        animate: animate,
        defaultRenderer: charts.ArcRendererConfig(
          arcWidth: arcwidth,
          startAngle: 4 / 5 * pi,
          arcLength: 7 / 5 * pi,
          arcRendererDecorators: null,
        ),
    );
  }
}
