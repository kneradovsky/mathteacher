import 'package:flutter/material.dart';
import 'package:mathteacher/LocalMessages.dart';
import 'package:mathteacher/RoundedButton.dart';
import 'package:mathteacher/core/Settings.dart';
import 'package:mathteacher/theme.dart';
import 'package:sqfentity_gen/sqfentity_gen.dart';

class SelectSettingsPage extends StatefulWidget {
  @override
  _SelectSettingsPageState createState() => _SelectSettingsPageState();
}

class _SelectSettingsPageState extends State<SelectSettingsPage> {

  CountThisLocalizations localStrings;
  Settings sett = Settings();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    localStrings = CountThisLocalizations.of(context);
  }

  @override
  Widget build(BuildContext context) {
    var textTheme = MathTextTheme(context);
    return Material(
        child: Container(
        decoration: BoxDecoration(color: Theme.of(context).backgroundColor),
    child: SafeArea(
      child: Column(
        children: <Widget>[
          Container(
              padding: EdgeInsets.only(top: 20, bottom: 20),
              child: Center(
                child: Text(localStrings.appbarSessionTypes, style: textTheme.bigLabel),
              )),
          Expanded(
            child: ListView.builder(
              itemBuilder: listItemBuilder,
            ),
          ),
           Container(
                padding: EdgeInsets.only(bottom: 20),
                child: Container(
                  alignment: Alignment.bottomCenter,
                  height: 100,
                  child: RoundedShadowButton(
                    key: Key("okButton"),
                    baseColor: Theme.of(context).accentColor,
                    shadowColor: Theme.of(context).primaryColorDark,
                    child: Text(localStrings.btnLabelBack, style: textTheme.buttonLabel),
                    onPressed: () => Navigator.of(context).pop(),
                  ),
                ),
              ),
        ],
      ),
    )));
  }

  Widget listItemBuilder(BuildContext ctx,int index) {
      var param = sett.getByInd(index);
      if(param==null) return null;
      var theme = Theme.of(ctx);
      bool active = sett.activeName == param.name;
      return  Card(
          elevation: active ? 2.0 : 0.0,
          color: active ? theme.accentColor : theme.splashColor,
          child: ListTile(
            onTap: () => setState((){sett.activeName=param.name;}),
            selected: false,
            title: Text(localStrings.parametersName[param.name]),
            subtitle: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    param.isTimed ? Icon(Icons.timer) : Icon(Icons.autorenew),
                    param.isTimed ? Text(localStrings.formatSeconds(param.seconds)) : Text(param.numTries.toString()),

                  ],),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(width: 30, alignment: Alignment.centerLeft,child: Text(param.operations.join(" "))),
                    Text("0"),
                    Icon(param.isSubZero ? Icons.arrow_downward : Icons.arrow_upward,color: active ? theme.selectedRowColor : theme.accentColor),
                  ],
                )
              ],
            ),
          ),

      );
  }
}
