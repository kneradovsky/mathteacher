import 'package:mathteacher/LocalMessages.dart';
import 'package:shared_preferences/shared_preferences.dart';

class _Settings {
  static final _Settings _instance = _Settings._();

  factory _Settings() {
    return _instance;
  }

  _Settings._() {
    _loadValues();
  }

  final String _namenNumTries="settings_numTries",_nameSeconds="settings_seconds",_nameIsTimed="settings_isTimed";
  final String _nameMaxNumber="settings_maxNumber",_nameIsSubZero="settings_isSubZero",_nameOperations="settings_operations";
  int _numTries = 10;
  int _seconds = 60;
  bool _isTimed = false;
  int _maxNumber = 10;
  bool _isSubZero = false;
  Set<String> _operations = Set.from(["+","-"]);

  int get numTries => _numTries;
  set numTries(int value) {
    _numTries=value;
    storeValue(_namenNumTries, _numTries);
  }

  int get seconds => _seconds;
  set seconds(int value) {
    _seconds=value;
    storeValue(_nameSeconds, _seconds);
  }

  bool get isTimed => _isTimed;
  set isTimed(bool value) {
    _isTimed=value;
    storeValue(_nameIsTimed, value);
  }

  int get maxNumber => _maxNumber;
  set maxNumber(int value) {
    _maxNumber=value;
    storeValue(_nameMaxNumber, value);
  }

  bool get isSubZero => _isSubZero;
  set isSubZero(bool value) {
    _isSubZero=value;
    storeValue(_nameIsSubZero, value);
  }

  List<String> get operations => _operations.toList(growable: false);

  void addOperation(String op) {
    _operations.add(op);
    storeValue(_nameOperations, _operations.toList(growable: false));
  }
  void removeOperation(String op) {
    _operations.remove(op);
    storeValue(_nameOperations, _operations.toList(growable: false));
  }

  static void storeValue(String name,dynamic value) {
    SharedPreferences.getInstance().then((prefs) {
      if(value is bool) prefs.setBool(name, value);
      else if(value is String) prefs.setString(name, value);
      else if(value is List<String>) prefs.setStringList(name, value);
      else prefs.setInt(name, value);
    });
  }
  _loadValues() {
    SharedPreferences.getInstance().then((prefs) {
      _isTimed = prefs.getBool(_nameIsTimed) ?? _isTimed;
      _seconds = prefs.getInt(_nameSeconds) ?? _seconds;
      _numTries = prefs.getInt(_namenNumTries) ?? _numTries;
      _maxNumber = prefs.getInt(_nameMaxNumber) ?? _maxNumber;
      _isSubZero = prefs.getBool(_nameIsSubZero) ?? _isSubZero;
      var strList = prefs.getStringList(_nameOperations);
      if(strList!=null)
        _operations = Set.from(strList);
    });
  }
}


class Parameters {
  final String name;
  final int numTries,maxNumber,seconds;
  final bool isTimed,isSubZero;
  Set<String> operations=Set.from(["+","-"]);

  Parameters(this.name,{this.numTries=0,this.maxNumber=0,this.seconds=0,this.isSubZero=false,this.isTimed=false,this.operations});
}

class Settings {
  static final Settings _instance = Settings._();
  String _activeName = '';

  Settings._() {
    initDataSource();
  }

  factory Settings() => _instance;

  Map<String,Parameters> _setlist = {
    CountThisLocalizations.paramsAdditionsTries : Parameters(CountThisLocalizations.paramsAdditionsTries,numTries: 10,maxNumber: 10,isSubZero: false,operations: Set.from(["+","-"])),
  CountThisLocalizations.paramsMultableTries : Parameters(CountThisLocalizations.paramsMultableTries,numTries: 10,maxNumber: 10,isSubZero: false,operations: Set.from(["*"])),
    CountThisLocalizations.paramsAdditionsTime : Parameters(CountThisLocalizations.paramsAdditionsTime,seconds: 60,isTimed: true,maxNumber: 10,isSubZero: false,operations: Set.from(["+","-"])),
    CountThisLocalizations.paramsMultableTime : Parameters(CountThisLocalizations.paramsMultableTime,seconds: 60,isTimed: true,maxNumber: 10,isSubZero: false,operations: Set.from(["*"])),
    CountThisLocalizations.paramsAdditionsTriesSubzero : Parameters(CountThisLocalizations.paramsAdditionsTriesSubzero,numTries: 10,maxNumber: 10,isSubZero: true,operations: Set.from(["+","-"])),
  };

  void initDataSource() async {
    var prefs = await SharedPreferences.getInstance();
    _activeName = prefs.getString('activeSettingsName');
    if(_activeName==null) _activeName = CountThisLocalizations.paramsAdditionsTime;
  }

  String get activeName => _activeName;
  set activeName(String val) {
    _activeName=val;
    SharedPreferences.getInstance().then((prefs) => prefs.setString("activeSettingsName", val));
  }

  Parameters getByName(String name) => _setlist[name];
  Parameters getByInd(int index) => index < _setlist.values.length ? _setlist.values.elementAt(index) : null;


  Parameters get active => _setlist[activeName];

  int get numTries => active.numTries;
  int get seconds => active.seconds;
  bool get isTimed => active.isTimed;
  int get maxNumber => active.maxNumber;
  bool get isSubZero => active.isSubZero;
  String get name => activeName;
  List<String> get operations => active.operations.toList(growable: false);

}