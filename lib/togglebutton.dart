
import 'package:flutter/material.dart';

class ToggleButton extends StatelessWidget {
  bool Function() predicate;
  Function() handlerOn, handlerOff;
  Widget child;
  String label="N/A";
  double size;
  ToggleButton({this.label,this.predicate,this.handlerOn,this.handlerOff,this.child, this.size=48});

  @override
  Widget build(BuildContext context) {
    Color shadowColor = predicate() ? Theme.of(context).colorScheme.primaryVariant : Theme.of(context).colorScheme.secondaryVariant;
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          boxShadow: [BoxShadow(color: shadowColor,offset: Offset(2,2),blurRadius: 2.0)]
      ),

      child: AnimatedCrossFade(
        duration: Duration(milliseconds: 200),
        crossFadeState:
        predicate() ? CrossFadeState.showFirst : CrossFadeState.showSecond,
        firstChild: Container(
            width: size,
            height: size,
            padding: EdgeInsets.all(0.0),
            child: RaisedButton(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
              color: Theme.of(context).accentColor,
              child: child == null
                  ? Text(label, style: Theme.of(context).textTheme.headline)
                  : child,
              padding: EdgeInsets.all(10.0),
              onPressed: handlerOn,
            )
        ),
        secondChild: Container(
          width: size,
          height: size,
          padding: EdgeInsets.all(0.0),
          child: RaisedButton(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            color: Theme.of(context).colorScheme.secondary,
            child: child == null ? Container(child: Text(label)) : child,
            padding: EdgeInsets.all(10.0),
            onPressed: handlerOff,
          ),
        ),
      ),
    );
  }
}