import 'dart:math';
import 'package:expressions/expressions.dart';
import 'package:mathteacher/core/Settings.dart';


class MathExpression {
  Expression _expression;
  ExpressionEvaluator eval = ExpressionEvaluator();
  MathExpression(String exp) {
    _expression = Expression.parse(exp);
  }
  String toString() => _expression.toString();
  int checkAnswer(num inAnswer) => inAnswer - answer;
  num get answer => eval.eval(_expression, {});
}

class ExpressionGenerator {
  Random _rnd = Random.secure();
  Settings settings = Settings();
  ExpressionGenerator();


  MathExpression get expression => _createExpression();

  String _generateExpression() {
    var opers = settings.operations.toList()..shuffle();
    var res = _rnd.nextInt(settings.maxNumber).toString() +
        opers.map((oper) => oper + _rnd.nextInt(settings.maxNumber).toString()).join();
    return res;
  }

  MathExpression _createExpression() {
    String res;
    MathExpression result;
    do {
      res = _generateExpression();
      result = MathExpression(res);
    } while (!settings.isSubZero && result.answer < 0);
    return result;
  }

}




