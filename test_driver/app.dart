import 'package:flutter_driver/driver_extension.dart';
import 'package:mathteacher/main.dart' as app;

void main() {
  enableFlutterDriverExtension();
  app.main();
}
