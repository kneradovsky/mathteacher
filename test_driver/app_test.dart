import 'dart:io';

import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

import 'package:expressions/expressions.dart';

takeScreenshot(FlutterDriver driver, String path) async {
  final List<int> pixels = await driver.screenshot();
  final File file = new File(path);
  await file.writeAsBytes(pixels);
  print(path);
}

void enterNumber(FlutterDriver driver, String number) async {
  for(int code in number.runes) {
    await driver.tap(find.byValueKey("numkey${String.fromCharCode(code)}"));
  }
}

void main() {
  group('Screenshots',() {
    FlutterDriver driver;
    final String screenshotFolder="./build/screenshots";
    new Directory(screenshotFolder).create(recursive: true);
    var eval = ExpressionEvaluator();

    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    tearDownAll(() async {
      if(driver!=null) driver.close();
    });

    test("mainPage",() async {
      await takeScreenshot(driver, screenshotFolder+"/screen-main.png");
    });

    test("settingsPage",() async {
      await driver.tap(find.byValueKey("settingsPage"));
      await takeScreenshot(driver, screenshotFolder+"/screen-settings.png");
      await driver.tap(find.byValueKey("okButton"));
    });

    test('find empty result', () async {
      await driver.tap(find.byValueKey("trainingPage"));
      await takeScreenshot(driver, screenshotFolder+"/screen-train-empty.png");
      await driver.tap(find.byValueKey("stopTraining"));
    });

    test('enter wrong value',() async {
      await driver.tap(find.byValueKey("trainingPage"));
      var expression = find.byValueKey("expression");
      var result = find.byValueKey("result");
      expect(await driver.getText(result),"??");
      String text = await driver.getText(expression);
      var exp = Expression.parse(text);
      num res = eval.eval(exp, {});
      String wrongAnswer = res.toString()+"0";
      await enterNumber(driver, wrongAnswer);
      await driver.tap(find.byValueKey("equalsButton"));
      await takeScreenshot(driver, screenshotFolder+"/screen-red.png");
      await driver.tap(find.byValueKey("stopTraining"));
    });

    test('enter right value',() async {
      await driver.tap(find.byValueKey("trainingPage"));
      var expression = find.byValueKey("expression");
      var result = find.byValueKey("result");
      expect(await driver.getText(result),"??");
      String text = await driver.getText(expression);
      var exp = Expression.parse(text);
      num res = eval.eval(exp, {});
      String rightAnswer = res.toString();
      await enterNumber(driver, rightAnswer);
      await driver.tap(find.byValueKey("equalsButton"));
      await takeScreenshot(driver, screenshotFolder+"/screen-green.png");
      await driver.tap(find.byValueKey("stopTraining"));
    });

    test('test stats',() async {
      await driver.tap(find.byValueKey("statsPage"));
      await takeScreenshot(driver, screenshotFolder+"/screen-stats-list.png");
      await driver.tap(find.byType("DataRow"));
      await takeScreenshot(driver, screenshotFolder+"/screen-stats-list.png");

    });



  });
}
